#include "forecast_places.h"
#include "forecast_config.h"
#include <ctype.h>

Eina_List *
forecast_places_categories(void)
{
   FILE *f;
   char buf[4096];
   Eina_List *list = NULL;

   snprintf(buf, sizeof(buf), "%s/places/countries.csv", MODULE_DIR);

   f = fopen(buf, "r");
   if (!f) return NULL;

   while ((fgets(buf, sizeof(buf), f)) != NULL)
     {
        char *name, *sep, *id = buf;
	size_t len;
        sep = strchr(id, ',');
        if (!sep) break;
        *sep = '\0';
        name = sep + 1;
        len = strlen(name);
        if (name[len-1] == '\n') name[len-1] = '\0';

        Places_Category *cat = malloc(sizeof(Places_Category));
        if (cat)
          {
             cat->id = atoi(id);
             snprintf(cat->name, sizeof(cat->name), "%s", name);
             list = eina_list_append(list, cat);
          }
     }
   fclose(f);
   return list;
}

#define N_FIELDS 4

Eina_List *
forecast_places_by_category(int id)
{
   FILE *f;
   char buf[4096];
   char *fields[N_FIELDS];
   Eina_List *list = NULL;

   snprintf(buf, sizeof(buf), "%s/places/%i.csv", MODULE_DIR, id);

   f = fopen(buf, "r");
   if (!f) return NULL;

   while ((fgets(buf, sizeof(buf), f)) != NULL)
     {
        char *cp = buf;
        int i = 0, n = 0;
        int len = strlen(cp);

        if (buf[len-1] == '\n') buf[len-1] = '\0';

        while (i < len)
          {
             if (buf[i] != ',')
               i++;
             else
               {
                  buf[i++] = '\0';
                  fields[n++] = cp;
                  cp = buf + i;
                  while ((*cp) && (*cp == ','))
                    {
                       if (n < (N_FIELDS - 2))
                         fields[n++] = NULL;
                       cp = buf + (++i);
                    }
               }
          }
        if (n < N_FIELDS)
          fields[n++] = cp;
        if (n != N_FIELDS) continue;

        Places_Location *loc = malloc(sizeof(Places_Location));
	if (loc)
          {
             if (fields[1] != NULL)
               snprintf(loc->name, sizeof(loc->name), "%s, %s", fields[0], fields[1]);
             else
               snprintf(loc->name, sizeof(loc->name), "%s", fields[0]);
             snprintf(loc->lat, sizeof(loc->lat), "%s", fields[2]);
             snprintf(loc->lon, sizeof(loc->lon), "%s", fields[3]);

             list = eina_list_append(list, loc);
          }
     }
   fclose(f);

   return list;
}
