#ifndef FORECAST_PLACES_H
#define FORECAST_PLACES_H

#include <Eina.h>

typedef struct Places_Category Places_Category;
struct Places_Category
{
   char name[128];
   int  id;
};

typedef struct Places_Location Places_Location;
struct Places_Location {
   char  name[128];
   char  region[128];
   char  lat[64];
   char  lon[64];
};

Eina_List *
forecast_places_categories(void);

Eina_List *
forecast_places_by_category(int id);

#endif
