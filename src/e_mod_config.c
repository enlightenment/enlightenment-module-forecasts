#include <e.h>
#include "e_mod_main.h"
#include "forecast_places.h"

struct _E_Config_Dialog_Data
{
   double           poll_time;
   int              display;
   int              degrees;
   int              location_id;
   char            *location;
   char            *lat;
   char            *lon;
   int              show_text;
   int              popup_on_hover;
   E_Config_Dialog *cfd;
   Evas_Object     *o_categories;
   Evas_Object     *o_locations;
};

static void        *_create_data(E_Config_Dialog *cfd);
static void         _free_data(E_Config_Dialog *cfd, E_Config_Dialog_Data *cfdata);
static Evas_Object *_basic_create_widgets(E_Config_Dialog *cfd, Evas *evas, E_Config_Dialog_Data *cfdata);
static int          _basic_apply_data(E_Config_Dialog *cfd, E_Config_Dialog_Data *cfdata);
static void         _fill_data(Config_Item *ci, E_Config_Dialog_Data *cfdata);

void
_config_forecasts_module(Config_Item *ci)
{
   E_Config_Dialog *cfd;
   E_Config_Dialog_View *v;
   char buf[4096];

   v = E_NEW(E_Config_Dialog_View, 1);

   v->create_cfdata = _create_data;
   v->free_cfdata = _free_data;
   v->basic.apply_cfdata = _basic_apply_data;
   v->basic.create_widgets = _basic_create_widgets;
   v->override_auto_apply = 1;

   snprintf(buf, sizeof(buf), "%s/module.edj",
            e_module_dir_get(forecasts_config->module));
   cfd =
     e_config_dialog_new(NULL, D_("Forecasts Settings"), "Forecasts", "_e_modules_forecasts_config_dialog", buf, 0, v, ci);
   forecasts_config->config_dialog = cfd;
}

static void
_fill_data(Config_Item *ci, E_Config_Dialog_Data *cfdata)
{
   cfdata->poll_time = (ci->poll_time / 60.0);
   cfdata->degrees = ci->degrees;
   if (ci->location)
     cfdata->location = strdup(ci->location);
   if (ci->lat)
     cfdata->lat = strdup(ci->lat);
   if (ci->lon)
     cfdata->lon = strdup(ci->lon);

   cfdata->show_text = ci->show_text;
   cfdata->popup_on_hover = ci->popup_on_hover;
   cfdata->location_id = ci->location_id;
}

static void *
_create_data(E_Config_Dialog *cfd)
{
   E_Config_Dialog_Data *cfdata;
   Config_Item *ci;

   ci = cfd->data;
   cfdata = E_NEW(E_Config_Dialog_Data, 1);

   _fill_data(ci, cfdata);
   return cfdata;
}

static void
_free_data(E_Config_Dialog *cfd EINA_UNUSED, E_Config_Dialog_Data *cfdata)
{
   if (!forecasts_config)
     return;

   forecasts_config->config_dialog = NULL;
   free(cfdata->lat);
   free(cfdata->lon);
   free(cfdata->location);
   free(cfdata);
   cfdata = NULL;
}

static void
_poll_time_slider_changed_cb(void *data, Evas_Object *obj,
                             void *event_info EINA_UNUSED)
{
   E_Config_Dialog_Data *cfdata = data;

   cfdata->poll_time = elm_slider_value_get(obj);

   e_config_dialog_changed_set(cfdata->cfd, 1);
}

static void
_radio_changed_cb(void *data, Evas_Object *obj EINA_UNUSED,
                  void *event_info EINA_UNUSED)
{
   E_Config_Dialog *cfd = data;

   e_config_dialog_changed_set(cfd, 1);
}

static char *
_text_location_get(void *data, Evas_Object *obj EINA_UNUSED, const char *part)
{
   Places_Location *location;

   if (strcmp(part, "elm.text")) return NULL;

   location = data;

   return strdup(location->name);
}

static void
_item_location_del(void *data, Evas_Object *obj EINA_UNUSED)
{
   Places_Location *location = data;

   free(location);
}

static void
_locations_selected_cb(void *data, Evas_Object *obj EINA_UNUSED,
                       void *event_info EINA_UNUSED)
{
   E_Config_Dialog_Data *cfdata;
   Elm_Object_Item *it;
   Places_Location *location;

   cfdata = data;
   it = event_info;

   location = elm_object_item_data_get(it);
   if (!location) return;

   cfdata->location = strdup(location->name);
   cfdata->lat = strdup(location->lat);
   cfdata->lon = strdup(location->lon);

   e_config_dialog_changed_set(cfdata->cfd, 1);
}

static void
_locations_fill(E_Config_Dialog_Data *cfdata)
{
   Eina_List *list;
   Evas_Object *glist;
   Places_Location *location;
   Elm_Object_Item *it, *sel = NULL;
   static Elm_Genlist_Item_Class itc;

   glist = cfdata->o_locations;

   itc.item_style = "no_icon";
   itc.func.content_get = NULL;
   itc.func.text_get = _text_location_get;
   itc.func.filter_get = NULL;
   itc.func.del = _item_location_del;
   elm_genlist_clear(glist);

   list = forecast_places_by_category(cfdata->location_id);
   EINA_LIST_FREE(list, location)
     {
        it = elm_genlist_item_append(glist, &itc, location, NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);
        if (!cfdata->lat || !cfdata->lon) continue;
        if ((!strcmp(location->lon, cfdata->lon)) && (!strcmp(location->lat, cfdata->lat)))
          sel = it;
     }

   if (sel)
     {
        elm_genlist_item_selected_set(sel, 1);
        elm_genlist_item_bring_in(sel, ELM_GENLIST_ITEM_SCROLLTO_MIDDLE);
     }
}

static void
_categories_selected_cb(void *data, Evas_Object *obj EINA_UNUSED,
                        void *event_info EINA_UNUSED)
{
   E_Config_Dialog_Data *cfdata;
   Elm_Object_Item *it;
   Places_Category *category;

   cfdata = data;
   it = event_info;
   category = elm_object_item_data_get(it);
   if (!category) return;

   cfdata->location_id = category->id;

   _locations_fill(cfdata);

   e_config_dialog_changed_set(cfdata->cfd, 1);
}

static void
_item_location_id_del(void *data, Evas_Object *obj EINA_UNUSED)
{
   Places_Category *category = data;
   free(category);
}

static char *
_text_location_id_get(void *data, Evas_Object *obj EINA_UNUSED, const char *part)
{
   Places_Category *category;

   if (strcmp(part, "elm.text")) return NULL;

   category = data;

   return strdup(category->name);
}

static Evas_Object *
_basic_create_widgets(E_Config_Dialog *cfd, Evas *evas,
                      E_Config_Dialog_Data *cfdata)
{
   Evas_Object *win, *o, *tb, *bx, *fr;
   Evas_Object *rec, *sl, *rg, *rd, *glist;
   Eina_List *list;
   Elm_Object_Item *it, *sel = NULL;
   Places_Category *category;
   static Elm_Genlist_Item_Class itc;
   int i = 0;

   win = cfd->dia->win;
   cfdata->cfd = cfd;

   o = tb = elm_table_add(win);
   evas_object_size_hint_align_set(tb, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_size_hint_weight_set(tb, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   evas_object_show(tb);

   fr = elm_frame_add(win);
   evas_object_size_hint_align_set(fr, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_size_hint_weight_set(fr, EVAS_HINT_EXPAND, 0);
   elm_object_text_set(fr, D_("Display Settings"));
   evas_object_show(fr);
   elm_table_pack(tb, fr, 0, 0, 1, 1);

   sl = elm_slider_add(win);
   evas_object_size_hint_align_set(sl, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_size_hint_weight_set(sl, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   elm_slider_min_max_set(sl, 1.0, 60.0);
   elm_slider_step_set(sl, 1.0 / (60.0 - 1.0));
   elm_slider_unit_format_set(sl, D_("%2.0f minutes"));
   elm_object_text_set(sl, D_("Poll Time"));
   elm_object_tooltip_text_set(sl, D_("Poll Time"));
   elm_slider_value_set(sl, cfdata->poll_time);
   evas_object_show(sl);
   evas_object_smart_callback_add(sl, "changed",
                                  _poll_time_slider_changed_cb, cfdata);
   elm_object_content_set(fr, sl);

   fr = elm_frame_add(win);
   evas_object_size_hint_align_set(fr, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_size_hint_weight_set(fr, EVAS_HINT_EXPAND, 0);
   elm_object_text_set(fr, D_("Unit Settings"));
   evas_object_show(fr);
   elm_table_pack(tb, fr, 0, 1, 1, 1);

   bx = elm_box_add(win);
   evas_object_size_hint_align_set(bx, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_size_hint_weight_set(bx, EVAS_HINT_EXPAND, 0); 
   evas_object_size_hint_weight_set(bx, 1.0, 1.0);
   evas_object_show(bx);
   elm_object_content_set(fr, bx);

   rg = rd = elm_radio_add(win);
   evas_object_size_hint_weight_set(rd, 1.0, 0);
   evas_object_size_hint_align_set(rd, 0.0, 1.0);
   elm_object_text_set(rd, D_("Celcius"));
   elm_radio_value_pointer_set(rd, &cfdata->degrees);
   elm_radio_state_value_set(rd, DEGREES_C);
   evas_object_show(rd);
   evas_object_smart_callback_add(rd, "changed",
                                  _radio_changed_cb, cfd);
   elm_box_pack_end(bx, rd);

   rd = elm_radio_add(win);
   evas_object_size_hint_weight_set(rd, 1.0, 0);
   evas_object_size_hint_align_set(rd, 0.0, 1.0);
   elm_object_text_set(rd, D_("Fahrenheit"));
   elm_radio_value_pointer_set(rd, &cfdata->degrees);
   elm_radio_state_value_set(rd, DEGREES_F);
   elm_radio_group_add(rd, rg);
   evas_object_show(rd);
   evas_object_smart_callback_add(rd, "changed",
                                  _radio_changed_cb, cfd);
   elm_box_pack_end(bx, rd);

   fr = elm_frame_add(win);
   evas_object_size_hint_align_set(fr, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_size_hint_weight_set(fr, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   elm_object_text_set(fr, D_("Location"));
   evas_object_show(fr);
   elm_table_pack(tb, fr, 0, 2, 1, 1);

   tb = elm_table_add(win);
   evas_object_size_hint_align_set(tb, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_size_hint_weight_set(tb, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   evas_object_show(tb);

   rec = evas_object_rectangle_add(evas);
   evas_object_size_hint_weight_set(rec, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   evas_object_size_hint_min_set(rec, 1, 120);
   elm_table_pack(tb, rec, 0, 0, 1, 1);
   elm_object_content_set(fr, tb);

   cfdata->o_categories = glist = elm_genlist_add(win);
   evas_object_size_hint_weight_set(glist, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   evas_object_size_hint_align_set(glist, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_scroller_policy_set(glist, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_AUTO);
   evas_object_show(glist);
   evas_object_smart_callback_add(glist, "selected", _categories_selected_cb, cfdata);
   elm_table_pack(tb, glist, 0, 0, 1, 1);

   itc.item_style = "no_icon";
   itc.func.content_get = NULL;
   itc.func.text_get = _text_location_id_get;
   itc.func.filter_get = NULL;
   itc.func.del = _item_location_id_del;

   list = forecast_places_categories();
   EINA_LIST_FREE(list, category)
     {
        it = elm_genlist_item_append(glist, &itc, category, NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);
        if (i++ == cfdata->location_id) sel = it;
     }

   rec = evas_object_rectangle_add(evas);
   evas_object_size_hint_weight_set(rec, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   evas_object_size_hint_min_set(rec, 1, 120);
   elm_table_pack(tb, rec, 0, 1, 1, 1);

   cfdata->o_locations = glist = elm_genlist_add(win);
   evas_object_size_hint_align_set(glist, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_size_hint_weight_set(glist, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   elm_scroller_policy_set(glist, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_AUTO);
   evas_object_show(glist);
   evas_object_smart_callback_add(glist, "selected", _locations_selected_cb, cfdata);
   elm_table_pack(tb, glist, 0, 1, 1, 1);

   if (sel)
     {
        elm_genlist_item_selected_set(sel, 1);
        elm_genlist_item_bring_in(sel, ELM_GENLIST_ITEM_SCROLLTO_MIDDLE);
        _locations_fill(cfdata);
     }
   //e_dialog_resizable_set(cfd->dia, 1);
   return o;
}

static int
_basic_apply_data(E_Config_Dialog *cfd, E_Config_Dialog_Data *cfdata)
{
   Config_Item *ci;

   ci = cfd->data;

   ci->degrees = cfdata->degrees;
   ci->poll_time = (cfdata->poll_time * 60.0);
   if (ci->location)
     eina_stringshare_del(ci->location);

   ci->show_text = cfdata->show_text;
   ci->popup_on_hover = cfdata->popup_on_hover;

   ci->location_id = cfdata->location_id;
   ci->location = eina_stringshare_add(cfdata->location);
   ci->lat = eina_stringshare_add(cfdata->lat);
   ci->lon = eina_stringshare_add(cfdata->lon);

   e_config_save_queue();
   _forecasts_config_updated(ci);
   return 1;
}

